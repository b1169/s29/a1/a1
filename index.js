const express = require('express');
const app = express();
const port = 4000;

const users = [{
	username: 'johndoe',
	password: 'johndoe1234'
}];

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/home', (request, response) =>

	response.send('Welcome to the homepage.')
)

app.get('/users', (request, response) => {

	let usernames = JSON.stringify(users)
	response.send(`${usernames}`)
})

app.delete('/delete-user', (request, response) =>{

	for (let i = 0; i<users.length; i++){

		if(request.body.name == users[i].name){
			

			delete users[i];
			//console.log(users);

			response.send(`User ${request.body.username} has been deleted.`)
		}

		else{
			response.send('User does not exist.')
		}
	}
})



app.listen(port, () => console.log(`Server is running at ${port}`))

